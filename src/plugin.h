#ifndef MSGHANDLER_PLUGIN_H
#define MSGHANDLER_PLUGIN_H

#ifndef Message_DEFINED
#define Message_DEFINED
  typedef struct message_t Message;
#endif

#ifndef PluginApi_DEFINED
#define PluginApi_DEFINED
  typedef struct pluginapi_t PluginApi;
#endif

#ifndef void_callback_DEFINED
#define void_callback_DEFINED
  typedef void (*void_callback)(void**);
#endif

/*
 * Allocates and returns a Message
 * Returns NULL on error
 */
Message *msg_construct(void *content);
/*
 * Creates a new reference to a Message object
 * Client code must call msg_destroy() on reference when no longer needed
 */
Message *msg_newref(Message *m);
/*
 * Deallocates a Message
 * Will set Message pointer to NULL on success
 */
void msg_destroy(Message **m);
/*
 * Enqueues a Message in a queue provided through InputPluginApi
 * Will fail if api_init() has not yet been successfully called
 * On success, sets Message pointer to NULL and returns nonzero
 * On failure, returns 0
 */
int msg_enqueue(Message** m, PluginApi* api);
/*
 * Returns a pointer to the content buffer of a Message
 */
void *msg_get_content(Message *m);
/*
 * Returns a reference to the header of the message
 */
char *msg_get_header(Message *m);
/*
 * Returns nonzero on success, zero on failure
 * 
 * Takes two arguments
 * destroy_content: a pointer to a function which can be used to deallocate
 * the content buffer of a Message
 * format_id: A unique string passed as an allocated buffer which is used by message dispatch
 * plugins to identify the source and structure of the message being dispatched
 */
int api_init(PluginApi* api, void_callback destroy_content, const char* message_header);

#endif