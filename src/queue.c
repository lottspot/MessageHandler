#include <time.h>
#include <malloc.h>
#include "queue.h"

typedef struct queue_implementation_t QueueImplementation;

struct messagehandler_queue_t {
  // The master queue should have id 0
  int id;
  QueueNode *first;
  QueueNode *last;
  QueueImplementation *impl;
};

struct queue_node_t {
  /* TODO: Include an alternate implementation using struct timeval
   * and gettimeofday() (OS X compatibility)
   * This will also require alternate implementations of stamp_node
   * and nodecmpr
   */
  struct timespec tstamp;
  Message *msg;
  struct queue_node_t *next;
};

QueueNode *node_construct(Message *m) {
  QueueNode *n = malloc(sizeof(QueueNode));
  if ( !n )
    return NULL;
  n->msg = m;
//  n->tstamp;
  n->next = NULL;
  return n;
}

Message *node_get_msg(QueueNode **n){
  if ( !*n )
    return NULL;
  Message *m = (*n)->msg;
  free(*n);
  *n = NULL;
  return m;
}

void node_stamp (QueueNode *n){
  if ( !n )
    return;
  clock_gettime(CLOCK_REALTIME, &(n->tstamp));
}

int q_get_id ( Queue *q ) {
  if ( !q )
    return -1;
  return q->id;
}


/*
 * 
 *  LEGACY NODE LIST SORTING METHODS
 * 
 */

/*
 * Return 1 if node1 is ordered before node2
 * Return -1 if node1 is ordered after node2
 * Return 0 if node1 and node2 are equal in order
 */
static int node_cmpr(QueueNode *n1, QueueNode *n2){
  if ( !n1 && !n2 )
    // Both NULL case
    return 0;
  if ( n1 && !n2 )
    // n2 is NULL; order n1 first
    return 1;
  if ( n2 && !n1 )
    // n1 is NULL; order n1 second
    return -1;
  struct timespec stamp1 = n1->tstamp;
  struct timespec stamp2 = n2->tstamp;
  time_t sec1 = stamp1.tv_sec;
  long nsec1 = stamp1.tv_nsec;
  time_t sec2 = stamp2.tv_sec;
  long nsec2 = stamp2.tv_nsec;
  if ( sec1 < sec2 ) {
    // node1 stamped before node2; ahead in order
    return 1;
  } else if (sec1 > sec2 ) {
    // node1 stamped after node2; behind in order
    return -1;
  } else {
    // Stamped in same second; check nanoseconds
    if (nsec1 < nsec2 ) {
      // node1 stamped before node2
      return 1;
    } else if ( nsec1 > nsec2) {
      // node1 stamped after node2
      return -1;
    } else {
      // Stamped in the same nanosecond; equal in order
      return 0;
    }
  }
}

static void nodelist_split(QueueNode *list, QueueNode **rear_half){
  if ( !list || !list->next ) {
    // 0 or 1 length list; no rear half
    *rear_half = NULL;
  } else {
    QueueNode *cursor_middle = list;
    QueueNode *cursor_end = list->next;
    // Advance cursor_end twice as fast as cursor_middle until it reaches the end of the list
    while ( cursor_end ) {
      cursor_end = cursor_end->next;
     if ( cursor_end ) {
	cursor_end = cursor_end->next;
	cursor_middle = cursor_middle->next;
      }
    }
    /* 
     * On even length lists, cursor_middle now points to half an element before the middle of the list
     * On odd length lists, cursor_middle now points to the center element
     * Advance cursor_middle by one more node to point it to the approximate rear half
     */
    *rear_half = cursor_middle->next;
    // Set endpoint of the first half of the list
    cursor_middle->next = NULL;
  }
}

static QueueNode *merge_sorted(QueueNode *list_a, QueueNode *list_b){
  QueueNode *front = NULL;
  // Base case
  if ( !list_a ) {
    return list_b;
  } else if ( !list_b ) {
    return list_a;
  }

  // Conditional assumes POSIX compliant time_t implementation
  if ( node_cmpr(list_a, list_b) >= 0 ) {
    // list_a was stamped before or same time as list_b; move ahead in queue
    front = list_a;
    front->next = merge_sorted(list_a->next, list_b);
  } else {
    // list_a was stampte after list_b; move behind in queue
    front = list_b;
    front->next = merge_sorted(list_b->next, list_a);
  }
  return front;
}

/* Main sorting method; implemented using Merge Sort algorithm */
static void nodelist_sort(QueueNode **n){
  QueueNode *head = *n;
  QueueNode *rear_half = NULL;
  // Base case
  if ( ! head || ! head->next ){
    return;
  }
  nodelist_split(head, &rear_half);
  nodelist_sort(&head);
  nodelist_sort(&rear_half);
  *n = merge_sorted(head, rear_half);
}

/* Wrapper method around nodelist_sort which accepts a Queue as an argument */
void q_sort(Queue **q){
  Queue *nodes = *q;
  if ( !nodes )
    return;
  nodelist_sort(&(nodes->first));
  QueueNode *last = nodes->first;
  if ( last ) {
    while ( last->next ) {
      last = last->next;
    }
  }
  nodes->last = last;
  *q = nodes;
}