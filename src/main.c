#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"
#include "plugin.h"
#include "api.h"

extern int counter = 0;

void destroy_content(void **content) { free(*content); *content = NULL; }

int dispatch_message(Message *m) {
  if ( ! m ) { 
    printf("You dispatched a NULL message\n");
    return 1;
  }
  if ( strncmp(msg_get_header(m), "supported", MSGHEADER_MAX) != 0 ) {
    return -1;
  }
  char* msg = (char*)msg_get_content(m);
  printf("%s\n", msg);
  msg_destroy(&m);
  return 1;
}

void input_message(PluginApi *api){
    int id = ++counter;
    /* malloc'd msg_header is not necessary
     * or recommended
     */
    char *msg_header = malloc(sizeof(char) * MSGHEADER_MAX);
    void_callback msg_destroy_content = destroy_content;
    if ( !msg_header ) {
      fprintf(stderr, "Failed to allocate memory for mesage header\n");
      exit(1);
    }
    strncpy(msg_header, "supported", MSGHEADER_MAX);
    if ( !api_init(api, msg_destroy_content, msg_header) ) {
      fprintf(stderr, "Error creating API for message %d\n", id);
      return;
    }
    free(msg_header);
    char *msg = malloc(sizeof(char) * 4096);
    sprintf(msg, "This is message %d", id);
    Message *m = msg_construct(msg);
    if ( id == 5 )
      msg_destroy(&m);
    msg_enqueue(&m, api);
}

int main(int argc, char **argv) {
    // Construct queues
    Queue *master = q_construct(0);
    Queue *input1 = q_construct(1);
    Queue *input2 = q_construct(2);
    Queue *input3 = q_construct(3);
    Queue *input4 = q_construct(4);
    Queue *input5 = q_construct(5);
    Queue *input6 = q_construct(6);
    // Define order in which to populate input queues
    Queue *queue_list[] = {input1, input2, input3, input4, input5, input6, NULL};
    PluginApi *api = NULL;
    int i;
    Queue *q = NULL;
    // Populate input queues
    for ( i = 0; queue_list[i]; i++) {
      q = queue_list[i];
      api = api_construct(q);
      input_message(api);
      api_destroy(&api);
    }
    // Screw up order;
    queue_list[3] = input1;
    queue_list[1] = input2;
    queue_list[5] = input3;
    queue_list[0] = input4;
    queue_list[4] = input5;
    queue_list[2] = input6;
    queue_list[6] = NULL;
    // Enqueue screwed up order on master
    QueueNode *nextnode = NULL;
    for ( i = 0; queue_list[i]; i++) {
      nextnode = q_dequeue(queue_list[i]);
      if ( !nextnode )
	fprintf(stderr, "Failed to dequeue message from index %d\n", i);
      if ( !q_enqueue(nextnode, master) )
	fprintf(stderr, "Failed to enqueue message from index %d\n", i);
    }
    // Fix the order with a merge sort
    q_sort(&master);
    // Dequeue master after sorting
    while ( q_has_message(master) ) {
      QueueNode *node = q_dequeue(master);
      if ( dispatch_message(node_get_msg(&node)) < 0 ) {
	printf("Error: Message format not supported by dispatcher.\n");
      return 1;
      }
    }
    // Break down queues
    for ( i = 0; queue_list[i]; i++) {
      q = queue_list[i];
      q_destroy(&q);
    }
    q_destroy(&master);
    return 0;
}
