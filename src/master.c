/*
 * Implement master data structures and functions to work with them
 */

#define PID_MAX 32768
#define QUEUE_MAX 4096

struct message_queue_t {
  int id;
  int pid;
  char *name;
  int (*queue_writer)(void*);
  int (**message_handlers)(void*);
  int message_handlers_len;
};

struct message_queue_t *msgq_id_index[QUEUE_MAX];
struct message_queue_t *msgq_pid_index[PID_MAX];
// TODO: Implement a hash table which provides access to message queues by name

// FIXME: implement as hash table
struct input_queue_plugin {
  int (*function_call)(void*);
  char *message_format;
  struct input_queue_plugin *next;
};

// FIXME: implement as hash table
struct message_dispatch_plugin {
  int (*function_call)(void*);
  struct message_dispatch_plugin *next;
};