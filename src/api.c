#include <malloc.h>
#include <pthread.h>
#include <string.h>
#include "api.h"

typedef long unsigned int refcount_t;

struct pluginapi_t {
  Queue *q;
  header_t message_header;
  void_callback msg_destroy_content;
  refcount_t refcount;
  pthread_mutex_t lock;
  pthread_mutexattr_t lattr;
  // TODO: Add any other fields needed from configuration
};

/* IMPLEMENT api.h */

PluginApi *api_construct(Queue *q) {
  PluginApi *api = malloc(sizeof(PluginApi));
  if ( !api )
    return NULL;
  api->msg_destroy_content = NULL;
  api->q = q;
  api->message_header[0] = '\0';
  api->refcount = 1;
  pthread_mutex_t *lockref = &(api->lock);
  pthread_mutexattr_t *lattrref = &(api->lattr);
  pthread_mutexattr_init(lattrref);
  pthread_mutexattr_settype(lattrref, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init(lockref, lattrref);
  return api;
}

void api_populate(PluginApi *api, void_callback msg_destroy_content, const char* message_header) {
 if ( !api )
    return;
  if ( !api->msg_destroy_content )
    api->msg_destroy_content = msg_destroy_content;
  if ( strnlen(api->message_header, MSGHEADER_MAX) < 1 )
    strncpy(api->message_header, message_header, MSGHEADER_MAX);
  return;
}

PluginApi *api_newref(PluginApi *api){
  pthread_mutex_t *lockref = &(api->lock);
  pthread_mutex_lock(lockref);
  if ( api )
    api->refcount++;
  pthread_mutex_unlock(lockref);
  return api;
}


void api_destroy(PluginApi **api){
  if ( !api )
    return;
  PluginApi *apiref = *api;
  if ( !apiref )
    return;
  pthread_mutex_t *lockref = &(apiref->lock);
  pthread_mutex_lock(lockref);
  apiref->refcount--;
  if ( !apiref->refcount ) {
    /* No more refs; destroy object
     NOTE: Does not destroy queue */
    pthread_mutexattr_t *lattrref = &(apiref->lattr);
    pthread_mutex_unlock(lockref);
    pthread_mutex_destroy(lockref);
    pthread_mutexattr_destroy(lattrref);
    free(apiref);
    *api = NULL;
    return;
  } else {
    // Object in use elsewhere; move on
    pthread_mutex_unlock(lockref);
    return;
  }
}

Queue *api_get_queue(PluginApi *api) {
  if ( !api ){
    return NULL;
  }
  return api->q;
}

char *api_get_messageheader(PluginApi *api){
  if ( !api )
    return NULL;
  char *header = api->message_header;
  return header;
}

void_callback api_get_contentdestructor(PluginApi *api){
  if ( !api )
    return NULL;
  return api->msg_destroy_content;
}