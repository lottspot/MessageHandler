#ifndef MSGHANDLER_QUEUE_H
#define MSGHANDLER_QUEUE_H

#ifndef Message_DEFINED
#define Message_DEFINED
  typedef struct message_t Message;
#endif

#ifndef QueueNode_DEFINED
#define QueueNode_DEFINED
  typedef struct queue_node_t QueueNode;
#endif

#ifndef Queue_DEFINED
#define Queue_DEFINED
  typedef struct messagehandler_queue_t Queue;
#endif

#ifndef InputPluginApi_DEFINED
#define InputPluginApi_DEFINED
  typedef struct input_pluginapi_t InputPluginApi;
#endif

/*
 * Takes a Message pointer, allocates a QueueNode, and attaches
 * Message to QueueNode. Returns pointer to allocated queue node
 * Method is implementation independent
 */
QueueNode *node_construct(Message *m);
/*
 * Takes QueueNode double pointer
 * Returns pointer to the Message object within the QueueNode
 * Deallocates QueueNode before returning Message
 * Method is implementation independent
 */
Message *node_get_msg(QueueNode **n);
/*
 * Sets timestamp on node to current system time
 */
void node_stamp(QueueNode *n);
/* 
 * Returns the id of a given queue
 * Method is implementation independent
 */
int q_get_id(Queue *q);
/*
 * Re-orders queue so that QueueNode bearing oldest timestamp
 * is in front and QueueNode bearing newest timestamp is in rear
 * Method is implementation independent
 */
void q_sort(Queue **q);
/*
 * Takes an integer to assign as a queue id
 * Allocates and returns a pointer to new Queue object
 * with specified id.
 * Work is performed in volatile memory only, therefore method is implementation independent
 */
Queue *q_construct(int id);
/*
 * Takes a Queue double pointer and for each QueueNode in the Queue
 * deallocates the QueueNode data, then deallocates the Node itself
 * Deallocates Queue object and sets Queue pointer to NULL
 */
void q_destroy(Queue **q);
/*
 * Returns true if queue contains a message, false otherwise.
 */
int q_has_message(Queue *q);
/*
 * Adds message to end of queue
 * Returns nonzero on success, zero on failure
 */
int q_enqueue(QueueNode *n, Queue *q);
/*
 * Removes the first message from the queue, returning a pointer to the removed message
 * Returns a NULL pointer if no more messages in queue
 */
QueueNode *q_dequeue(Queue *q);
#endif