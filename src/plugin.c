#include <malloc.h>
#include <pthread.h>
#include <string.h>
#include "plugin.h"
#include "api.h"
#include "queue.h"

#ifndef MessageMeta_DEFINED
#define MessageMeta_DEFINED
  typedef struct message_meta_t MessageMeta;
#endif
typedef long unsigned int refcount_t;

struct message_t {
  /* NOTE: The message constructor and destructor do not manage the char* memory */
  void *content;
  PluginApi *api;
  refcount_t refcount;
  pthread_mutex_t lock;
  pthread_mutexattr_t lattr;
};

Message *msg_construct(void *content){
  Message *m = malloc(sizeof(Message));
  if ( !m )
    return NULL;
  m->api = NULL;
  m->content = content;
  m->refcount = 1;
  pthread_mutex_t *lockref = &(m->lock);
  pthread_mutexattr_t *lattrref = &(m->lattr);
  pthread_mutexattr_init(lattrref);
  pthread_mutexattr_settype(lattrref, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init(lockref, lattrref);
  return m;
}

Message *msg_newref(Message *m){
  if ( m ) {
    pthread_mutex_t *lockref = &(m->lock);
    pthread_mutex_lock(lockref);
    m->refcount++;
    api_newref(m->api);
    pthread_mutex_unlock(lockref);
  }
  return m;
}


void msg_destroy(Message **m) {
  if ( ! *m ) 
    return;
  Message *mref = *m;
  PluginApi *api = mref->api;
  void_callback destroy_content;
  if ( ! api )
    return;
  destroy_content = api_get_contentdestructor(api);
  if ( ! destroy_content ) 
    return;
  void *content = mref->content;
  pthread_mutex_t *lockref = &(mref->lock);
  pthread_mutex_lock(lockref);
  mref->refcount--;
  if ( !mref->refcount ) {
    // Object is no longer in use; destroy
    pthread_mutexattr_t *lattrref = &(mref->lattr);
    destroy_content(&content);
    api_destroy(&api);
    pthread_mutex_unlock(lockref);
    pthread_mutex_destroy(lockref);
    pthread_mutexattr_destroy(lattrref);
    free(*m);
    *m = NULL;
    return;
  }
  pthread_mutex_unlock(lockref);
  *m = NULL;
  return;
}

int msg_enqueue(Message **m, PluginApi *api){
  if ( !api || !*m )
    return 0;
  Queue *q = api_get_queue(api);
  if ( ! q )
    return 0;
  (*m)->api = api_newref(api);
  QueueNode *n = NULL;
  n = node_construct(*m);
  int xstat = q_enqueue(n, q);
  if (xstat){
    node_stamp(n);
    *m = NULL;
  }
  return xstat;
}

void *msg_get_content(Message *m) {
  if ( !m )
    return NULL;
  return m->content;
}

char *msg_get_header(Message *m) {
  if ( !m )
    return NULL;
  return api_get_messageheader(m->api);
}

int api_init(PluginApi* api, void_callback destroy_content, const char* message_header) {
  // TODO: Improve header error checking; check that string represents valid c type
  if ( !api || strnlen(message_header, MSGHEADER_MAX) < 1 || !destroy_content )
    return 0;
  api_populate(api, destroy_content, message_header);
  return 1;
}