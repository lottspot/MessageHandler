#ifndef MSGHANDLER_PLUGIN_MANAGER_H
#define MSGHANDLER_PLUGIN_MANAGER_H

#ifndef Queue_DEFINED
#define Queue_DEFINED
  typedef struct messagehandler_queue_t Queue;
#endif

#ifndef PluginApi_DEFINED
#define PluginApi_DEFINED
  typedef struct pluginapi_t PluginApi;
#endif

#ifndef Message_DEFINED
#define Message_DEFINED
  typedef struct message_t Message;
#endif

#ifndef void_callback_DEFINED
#define void_callback_DEFINED
  typedef void (*void_callback)(void**);
#endif

#ifndef MSGHEADER_MAX
#define MSGHEADER_MAX 256
#endif

#ifndef header_t_DEFINED
#define header_t_DEFINED
  typedef char header_t[MSGHEADER_MAX];
#endif

/* 
 * Constructor for PluginApi object
 * New object must be associated with a queue
 */
PluginApi *api_construct(Queue *q);
/*
 * Tags the provided api object with input plugin data
 * Does not overwrite existing tags
 */
void api_populate(PluginApi* api, void_callback msg_destroy_content, const char* message_header);
/*
 * Return a copy of a reference to a PluginApi
 * References to API objects should not be passed directly between objects 
 * and should instead be passed through this method.
 */
PluginApi *api_newref(PluginApi *api);
/*
 * deallocates a PluginApi; sets pointer to NULL
 * 
 */
void api_destroy(PluginApi **api);
/*
 * Return a pointer to the queue contained in a PluginApi
 */
Queue *api_get_queue(PluginApi *api);
/*
 * Returns a reference to the api's message header.
 * Client code should not deallocate the message header
 */
char *api_get_messageheader(PluginApi *api);
/*
 * Returns the destroy_conent function from the
 * provided api object
 */
void_callback api_get_contentdestructor(PluginApi *api);
#endif