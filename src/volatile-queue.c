#include <pthread.h>
#include "queue.c"

// linked in plugin.c
extern void *msg_destroy(Message **m);

typedef long unsigned int refcount_t;

struct queue_implementation_t {
  refcount_t refcount;
  pthread_mutex_t lock;
  pthread_mutexattr_t lattr;
};

Queue *q_construct(int id){
  if ( id < 0 )
    return NULL;
  Queue *q = malloc(sizeof(Queue));
  if ( !q )
    return NULL;
  QueueImplementation *impl = malloc(sizeof(QueueImplementation));
  if ( !impl ) {
    free(q);
    return NULL;
  }
  pthread_mutex_t *lockref = &(impl->lock);
  pthread_mutexattr_t *lattrref = &(impl->lattr);
  pthread_mutexattr_init(lattrref);
  pthread_mutexattr_settype(lattrref, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init(lockref, lattrref);
  q->impl = impl;
  q->id = id;
  q->first = NULL;
  q->last = NULL;
  q->impl->refcount = 1;
  return q;
}

Queue *q_newref(Queue *q){
  if ( q ) {
    QueueImplementation *impl = q->impl;
    if ( !impl )
      return NULL;
    pthread_mutex_t *lockref = &(impl->lock);
    pthread_mutex_lock(lockref);
    impl->refcount++;
    pthread_mutex_unlock(lockref);
  }
  return q;
}

void q_destroy(Queue **q) {
  if ( ! *q ) 
    return;
  QueueImplementation *impl = (*q)->impl;
  if ( !impl )
    return;
  QueueNode *n = NULL;
  Message *m = NULL;
  pthread_mutex_t *lockref = &(impl->lock);
  pthread_mutexattr_t *lattrref = &(impl->lattr);
  pthread_mutex_lock(lockref);
  impl->refcount--;
  if ( impl->refcount ) {
    // Queue is still in use. Move along.
    pthread_mutex_unlock(lockref);
  } else {
    for (n = q_dequeue(*q); n != NULL; n = q_dequeue(*q)) {
      m = node_get_msg(&n);
      msg_destroy(&m);
    }
    pthread_mutex_unlock(lockref);
    pthread_mutex_destroy(lockref);
    pthread_mutexattr_destroy(lattrref);
    free((*q)->impl);
    free(*q);
    *q = NULL;
  }
  return;
}

int q_has_message(Queue *q) {
  if ( !q )
    return 0;
  if ( !q->first )
    return 0;
  return 1;
}

int q_enqueue(QueueNode *n, Queue *q) {
  if ( ! n || ! q ) 
    return 0;
  pthread_mutex_t *lockref = &(q->impl->lock);
  pthread_mutex_lock(lockref);
  if ( q->last == NULL ) {
    q->first = n;
    q->last = n;
  } else {
    q->last->next = n;
    q->last = n;
  }
  pthread_mutex_unlock(lockref);
  return 1;
}

QueueNode *q_dequeue(Queue *q) {
  if ( ! q ) 
    return NULL;
  pthread_mutex_t *lockref = &(q->impl->lock);
  pthread_mutex_lock(lockref);
  QueueNode *n = NULL;
  if ( q->first != NULL ) {
    n = q->first;
    q->first = n->next;
    n->next = NULL;
    if ( q->first == NULL ) {
      q->last = NULL;
    }
  }
  pthread_mutex_unlock(lockref);
  return n;
}